.PHONY: test
test:
	python -m unittest discover tests

.PHONY: checkstyle
checkstyle:
	pycodestyle --show-source presenters tests