# Presenters

---

![](https://app.codeship.com/projects/6c4d9930-ae79-0135-8b8b-0ee911c4d4c3/status?branch=master "CI status")

## Abstract

Presenters is an interactive application to choose random presenters at meetings.

The application supports:

+ adding and removing potential presenters, 
+ marking potential presenters available or unavailable,
+ choosing a random presenter amongst the ones available for the current meeting.

## Installation

The application is shipped as a Python 3.6 package.
It depends on the `virtualenv` and `python3.6` binaries which you might need to install, if needed.
To install the application and its dependencies clone the repository, navigate to its main folder
and run the following commands:

```Bash
virtualenv -p python3.6 venv
source venv/bin/activate
pip install -r requirements.txt
pip install .
```

Finally, to run the application:

```Bash
presenters_app
```

## User Guide

The application is an interactive interpreter of simple commands
that manipulate a set of presenters and a set of unavailability logs.

### Commands

All commands are of the form:

    command-name [parameter1] [, parameter2, ...]

Please, mind the spaces and the commas!
Notice that the command-name is separated by its parameters by a space, while all other
parameters are separated by a comma followed by a space. The application is not too strict
about the whitespace preceding and following commas but don't play around with it too much.

The squared parentheses denote optionality, thus commands can be classified in three categories:

1. [single commands] which only consist of a command name,
2. [one parameter commands] which consist of a command name followed by a single parameter,
3. [multi parameter commands] which consist of a command name followed by two or more parameters.

For instance,

1. `list-presenters` is a single command,
2. `add Andrea Aquino` is a single parameter command,
3. `remove Andrea Aquino, Alan Turing` is a multi parameter command.

To see the full list of commands please use the `help` single command.

### Persistent Storage

After the user executes a command, the application saves its state to two files:

1. `presenters/resources/presenters.txt`, which holds the list of presenters,
2. `presenters/resources/unavailability_logs.txt`, which holds the unavailability logs.

The resources folder and both files are created the first time the application is ran.

The application always restores its last state by reading these two files on start up.
While ill advised, it is still possible to manually modify these files before or after running the application.
All manual modifications to these files operated while the application is running are likely to be overwritten
on the execution of the next command.

The application allows its user to clear one or both files on demand.
This can be useful depending on the target usage scenario for the application.
For instance, if there are often different attendees to meetings, then it could be useful to clear all presenters
stored by the application at the start of a meeting and manually (re)insert the current attendees. If they rarely
change, then it could be better never to clear them.

Notice that the application does not clear the unavailability logs of a presenter when that presenter is removed.
This is intended behaviour: the unavailability logs will remain as witnesses of the absence of that presenter in the
past; if the presenter is then added again its logs will be reloaded and active. This also ensures that deleting a
presenter by mistake does not hinder her unavailability logs history.

Unavailability logs can be also fully cleared but, since this operation is permanent, we suggest either not to do that
at all, or to create a copy of the relevant file prior to clearing it. We suggest to remove erroneous unavailability
logs one by one by using the `mark-available` command.

## Tests and Code Style

The application is shipped with a Makefile to run tests and check PEP8 compliancy.
Please refer to the `test` and `checkstyle` targets in the Makefile.

```Bash
make test
make checkstyle
```

## Logo

The application logo has been designed by [Icon Pond](https://www.flaticon.com/authors/popcorns-arts) 
at [flaticon](http://www.flaticon.com).
