from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='presenters',
    version='1.0.0',
    description='An application to choose random presenters at meetings',
    long_description=readme(),
    keywords='meeting application presenter random',
    url='https://bitbucket.org/andryak/presenters',
    author='Andrea Aquino',
    author_email='andrex.aquino@gmail.com',
    license='GNU GPL v3',
    packages=['presenters'],
    entry_points={
      'console_scripts': ['presenters_app=presenters.presenters:run_interactive_app'],
    },
    include_package_data=True,
    zip_safe=False
)
