# Presenters Application Design Choices

---

## Requirements

The application must be implemented using an object oriented programming language.
It must allow to randomly select a presenter for a meeting and it must:

1. be usable, it should take less than 5 seconds to set it up and use it,
2. allow to exclude some people from the list of participants to the current meeting,
3. allow to add/remove participants,
4. keep track of people excluded from the list of participants to the current meeting (storing the person and the date).

## Programming Language

I opted for the Python programming language because it is an object oriented
language particularly suited for fast prototyping of small applications.

In particular, I choose version 3.6 of the language because it is the latest
release and includes a few useful features I used in the project:

1. formatted string literals, which are easier to read than format strings,
2. more efficient implementation of dictionaries and sets, which are extensively used by the application,
3. better handling of UTF-8 encoded strings.

Please, [check the docs](https://docs.python.org/3/whatsnew/3.6.html) for more information.

### Differences from a Java Implementation

The most dramatic differences from a potential Java implementation of the same program consist in the code
style, the project organisation and the application of some design patterns.

Variables and class attributes in Python programs are usually written using the *slug_notation* rather than
the *camelCaseNotation*. This is reflected by my implementation.

Python programs are usually much smaller then corresponding Java programs due to the hybrid nature of the
Python language. The programmer can choose an imperative, functional or object oriented programming style
in different parts of the project, thus largely simplifying the implementation of different code blocks.
I opted for an imperative style in the main method of the `presenters.py` application, an object oriented
style for the definition of the objects manipulated by the application (presenters and unavailability logs)
and a functional programming style for the implementation of many methods.

I chose to implement `Presenter` and `UnavailabilityLog` as classes instead of namedtuples to better match
the requirements of the project, but technically the use of namedtuples could have produced a large saving
in terms of lines of code while achieving the same goal.

Python projects tend to have a much shallower structure than equivalent Java projects, to ease the readability of
imports. This is exactly the case for this project which only includes two modules: `presenters` and `tests`.

In Java it is common practice to access class attributes with getters and setters. This is not the case for
Python programs, in which these attributes are accessed using the "dot notation". In fact, in Python all "dot"
accesses to class attributes can be mediated by means of getter or setter functions at later stages of the
development of a project by using "properties". Because of this Python programmers prefer a simpler and clearer
programming style based on dot notation and switch to properties if an architectural redesign of the application
makes it important to mediate the access to class attributes by means of getters and setters. For this reasons,
in this project I made extensive use of the dot notation without hurting the project resiliency to architectural
changes.

## Version Control

I implemented a first draft of the project on my local machine and then uploaded everything on a *BitBucket* repository.
Since the first commit, I kept the commit history consistent and well documented.
The branches logs reveal that I used a few different branches to implement some features.
This might be considered overkilling for a project of this size but I did not want to clutter the master branch with
feature-specific commits.

## Issue Tracking

I created, managed and resolved four issues publicly available in the BitBucket project's issue tracker.
Each issue is marked with the proper commit log hash corresponding to its resolution for easier tracking.

## Continuous Integration

I implemented a CI workflow for the project by means of *CodeShip* and *BitBucket*.
This might be considered overkilling for a project of this size but is nice to have.

## Persistent Storage

In this project I stored persistent data in files rather then in a database.
I considered both options and finally decided that a database would have considerably increased the setup time of the
application, hindering the "simplicity requirement" I was given.

Since the application is functional in nature (that is, all accesses to the persistent storage are synchronous), it
would be easy to integrate a database with two tables in place of the two files currently used by the application.
If necessary, this could be easily achieved by means of the SQLAlchemy Python library, which is an ORM framework
supporting a large variety of databases ranging from SQLite, to Postgres, to MySQL, and so on.
