import unittest

from presenters.presenters import Presenter
from presenters.presenters import ULog
from datetime import date
from tests.utils import make_ulog


class TestUnavailabilityLog(unittest.TestCase):
    def test_init(self):
        presenter = Presenter('Andrea Aquino')
        date_ = date(1990, 1, 30)
        ulog = ULog(presenter, date_)
        self.assertEqual(ulog.presenter, presenter)
        self.assertEqual(ulog.date_, date_)

    def test_ulogs_are_equal(self):
        ulog1 = make_ulog('Andrea Aquino', '1990-01-30')
        ulog2 = make_ulog('Andrea Aquino', '1990-01-30')
        self.assertIsNot(ulog1, ulog2)
        self.assertEqual(ulog1, ulog2)

    def test_ulogs_are_not_equal(self):
        ulog1 = make_ulog('Alan Turing', '1990-01-30')
        ulog2 = make_ulog('Andrea Aquino', '1990-02-01')
        self.assertIsNot(ulog1, ulog2)
        self.assertNotEqual(ulog1, ulog2)

    def test_ulogs_are_not_equal_because_of_presenter(self):
        ulog1 = make_ulog('Alan Turing', '1990-01-30')
        ulog2 = make_ulog('Andrea Aquino', '1990-01-30')
        self.assertIsNot(ulog1, ulog2)
        self.assertNotEqual(ulog1, ulog2)

    def test_ulogs_are_not_equal_because_of_date(self):
        ulog1 = make_ulog('Andrea Aquino', '1990-02-01')
        ulog2 = make_ulog('Andrea Aquino', '1990-01-30')
        self.assertIsNot(ulog1, ulog2)
        self.assertNotEqual(ulog1, ulog2)

    def test_ulogs_have_same_hash(self):
        ulog1 = make_ulog('Andrea Aquino', '1990-01-30')
        ulog2 = make_ulog('Andrea Aquino', '1990-01-30')
        self.assertIsNot(ulog1, ulog2)
        self.assertEqual(hash(ulog1), hash(ulog2))

    def test_serialize(self):
        ulog = make_ulog('Andrea Aquino', '1990-01-30')
        self.assertEqual(ulog.serialize(), 'Andrea Aquino\n1990-01-30')

    def test_deserialize(self):
        expected_ulog = make_ulog('Andrea Aquino', '1990-01-30')
        actual_ulog = ULog.deserialize('Andrea Aquino\n1990-01-30')
        self.assertEqual(actual_ulog, expected_ulog)

    def test_metamorphic_serialize_deserialize(self):
        ulog1 = make_ulog('Andrea Aquino', '1990-01-30')
        ulog2 = ULog.deserialize(ulog1.serialize())
        self.assertIsNot(ulog1, ulog2)
        self.assertEqual(ulog1, ulog2)

    def test_ulog_to_string(self):
        ulog = make_ulog('Andrea Aquino', '1990-01-30')
        self.assertEqual(str(ulog), 'Andrea Aquino unavailable on 1990-01-30')

    def test_ulog_repr(self):
        ulog = make_ulog('Andrea Aquino', '1990-01-30')
        self.assertEqual(repr(ulog), "ULog(presenter='Andrea Aquino', date=datetime.date(1990, 1, 30))")


if __name__ == '__main__':
    unittest.main()
