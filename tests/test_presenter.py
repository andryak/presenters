import unittest

from presenters.presenters import Presenter


class TestPresenter(unittest.TestCase):
    def test_init(self):
        presenter = Presenter('Andrea Aquino')
        self.assertEqual(presenter.name, 'Andrea Aquino')

    def test_presenters_are_equal(self):
        presenter1 = Presenter('Andrea Aquino')
        presenter2 = Presenter('Andrea Aquino')
        self.assertIsNot(presenter1, presenter2)
        self.assertEqual(presenter1, presenter2)

    def test_presenters_are_not_equal(self):
        presenter1 = Presenter('Andrea Aquino')
        presenter2 = Presenter('Alan Turing')
        self.assertIsNot(presenter1, presenter2)
        self.assertNotEqual(presenter1, presenter2)

    def test_presenters_have_same_hash(self):
        presenter1 = Presenter('Andrea Aquino')
        presenter2 = Presenter('Andrea Aquino')
        self.assertIsNot(presenter1, presenter2)
        self.assertEqual(hash(presenter1), hash(presenter2))

    def test_serialize(self):
        presenter = Presenter('Andrea Aquino')
        self.assertEqual(presenter.serialize(), 'Andrea Aquino')

    def test_deserialize(self):
        presenter = Presenter('Andrea Aquino')
        self.assertEqual(Presenter.deserialize('Andrea Aquino'), presenter)

    def test_metamorphic_serialize_deserialize(self):
        presenter1 = Presenter('Andrea Aquino')
        presenter2 = Presenter.deserialize(presenter1.serialize())
        self.assertIsNot(presenter1, presenter2)
        self.assertEqual(presenter1, presenter2)

    def test_presenter_to_string(self):
        presenter = Presenter('Andrea Aquino')
        self.assertEqual(str(presenter), 'Andrea Aquino')

    def test_presenter_repr(self):
        presenter = Presenter('Andrea Aquino')
        self.assertEqual(repr(presenter), repr('Andrea Aquino'))


if __name__ == '__main__':
    unittest.main()
