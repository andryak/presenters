from presenters.presenters import App
from presenters.presenters import Presenter
from presenters.presenters import ULog
from datetime import datetime


def make_ulog(presenter_name, date_str):
    """Create an unavailability log with less boilerplate.

    Args:
        presenter_name (str): The presenter name.
        date_str (str): A date in yyyy-mm-dd format.

    Returns:
        (:obj:UnavailabilityLog): An unavailability log for the given presenter and date.
    """
    presenter = Presenter(presenter_name)
    date_ = datetime.strptime(date_str, "%Y-%m-%d").date()
    return ULog(presenter, date_)


def make_app():
    """Create an application with less boilerplate.

    Create an application with three presenters, two of which unavailable
    in specific days in the past and one always available.

    Presenters:

      1. Andrea Aquino was unavailable on 1990-01-30,
      2. Alan Turing was unavailable on 2012-12-21,
      3. Alonzo Church has always been available.

    Returns:
        (:obj:App): An application for test purposes.
    """
    presenters = {
        Presenter('Andrea Aquino'),
        Presenter('Alan Turing'),
        Presenter('Alonzo Church'),
    }

    logs = {
        make_ulog('Andrea Aquino', '1990-01-30'),
        make_ulog('Alan Turing', '2012-12-21'),
    }

    return App(presenters, logs)
