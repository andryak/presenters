import tempfile
import unittest

from presenters.presenters import Presenter
from presenters.presenters import ULog
from presenters.presenters import App
from datetime import date
from tests.utils import make_app
from tests.utils import make_ulog


class TestApp(unittest.TestCase):
    def test_init(self):
        presenters, ulogs = set(), set()
        app = App(presenters, ulogs)
        self.assertIs(app.presenters, presenters)
        self.assertIs(app.ulogs, ulogs)

    def test_reset(self):
        app = make_app()
        app.reset()
        self.assertEqual(app.presenters, set())
        self.assertEqual(app.ulogs, set())

    def test_reset_presenters(self):
        app = make_app()
        app.reset_presenters()
        self.assertEqual(app.presenters, set())
        self.assertNotEqual(app.ulogs, set())

    def test_reset_ulogs(self):
        app = make_app()
        app.reset_unavailability_logs()
        self.assertNotEqual(app.presenters, set())
        self.assertEqual(app.ulogs, set())

    def test_presenter_exists(self):
        app = make_app()
        presenter = Presenter('Andrea Aquino')
        self.assertTrue(app.presenter_exists(presenter))

    def test_presenter_does_not_exist(self):
        app = make_app()
        presenter = Presenter('Steve Jobs')
        self.assertFalse(app.presenter_exists(presenter))

    def test_ulog_exists(self):
        app = make_app()
        ulog = make_ulog('Andrea Aquino', '1990-01-30')
        self.assertTrue(app.ulog_exists(ulog))

    def test_ulog_does_not_exist(self):
        app = make_app()
        ulog = make_ulog('Bill Gates', '1990-01-30')
        self.assertFalse(app.ulog_exists(ulog))

    def test_presenters_unavailable_today(self):
        today = date.today()
        presenter = Presenter('Steve Jobs')

        app = App(
            presenters=set([presenter]),
            ulogs=set([ULog(presenter, today)]),
        )

        # Notice for debuggers:
        # This test can fail if it starts its execution before midnight and reaches this point after midnight.
        # Given the probability of the event this should not be an issue.
        #
        # This problem can be solved virtualizing time, possibly by monkey patching the date module.
        unavailable_presenters = app.presenters_unavailable_today()
        self.assertIn(presenter, unavailable_presenters)
        self.assertEqual(len(unavailable_presenters), 1)

    def test_presenters_available_today(self):
        today = date.today()
        presenter1 = Presenter('Andrea Aquino')
        presenter2 = Presenter('Steve Jobs')  # This guy is unavailable today.

        app = App(
            presenters={presenter1, presenter2},
            ulogs=set([ULog(presenter2, today)]),
        )

        # Notice for debuggers:
        # This test can fail if it starts its execution before midnight and reaches this point after midnight.
        # Given the probability of the event this should not be an issue.
        #
        # This problem can be solved virtualizing time, possibly by monkey patching the date module.
        available_presenters = app.presenters_available_today()
        self.assertIn(presenter1, available_presenters)
        self.assertEqual(len(available_presenters), 1)

    def test_dump_load_presenters(self):
        app = make_app()
        with tempfile.NamedTemporaryFile('w+') as file:
            # Write presenters to the file.
            app.dump_presenters(file)

            # Reset cursor to the beginning of the file and read back all presenters.
            file.seek(0)
            presenters = App.load_presenters(file)
        self.assertEqual(app.presenters, presenters)

    def test_dump_load_ulogs(self):
        app = make_app()
        with tempfile.NamedTemporaryFile('w+') as file:
            # Write presenters to the file.
            app.dump_unavailability_logs(file)

            # Reset cursor to the beginning of the file and read back all presenters.
            file.seek(0)
            ulogs = App.load_unavailability_logs(file)
        self.assertEqual(app.ulogs, ulogs)

    def test_random_presenter_with_no_presenters(self):
        app = App(
            presenters=set(),
            ulogs=set(),
        )

        with self.assertRaises(IndexError):
            app.random_presenter()

    def test_random_presenter_with_some_available_presenters(self):
        app = make_app()
        presenter = app.random_presenter()
        self.assertTrue(app.presenter_exists(presenter))

    def test_random_presenter_with_no_available_presenters(self):
        app = make_app()

        for presenter in app.presenters:
            app.mark_unavailable(presenter)

        # Notice for debuggers:
        # This test can fail if it starts its execution before midnight and reaches this point after midnight.
        # Given the probability of the event this should not be an issue.
        #
        # This problem can be solved virtualizing time, possibly by monkey patching the date module.
        with self.assertRaises(IndexError):
            app.random_presenter()

    def test_mark_unavailable(self):
        # Notice for debuggers:
        # This test can fail if the two calls to `app.available_today` happen on different days.
        # Given the probability of the event this should not be an issue.
        app = make_app()

        presenter = Presenter('Alonzo Church')
        self.assertIn(presenter, app.presenters_available_today())

        app.mark_unavailable(presenter)
        self.assertNotIn(presenter, app.presenters_available_today())

    def test_mark_available(self):
        # Notice for debuggers:
        # This test can fail if the calls to `app.available_today` happen on different days.
        # Given the probability of the event this should not be an issue.
        app = make_app()

        presenter = Presenter('Alonzo Church')
        app.mark_unavailable(presenter)
        self.assertNotIn(presenter, app.presenters_available_today())

        app.mark_available(presenter)
        self.assertIn(presenter, app.presenters_available_today())


if __name__ == '__main__':
    unittest.main()
