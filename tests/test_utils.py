import unittest

from presenters.utils import parse_query


class TestUtils(unittest.TestCase):
    def test_empty_query(self):
        self.assertEqual(parse_query(''), ('', []))

    def test_single_command(self):
        self.assertEqual(parse_query('cmd'), ('cmd', []))

    def test_command_with_one_param(self):
        self.assertEqual(parse_query('cmd 1'), ('cmd', ['1']))

    def test_command_with_many_params(self):
        self.assertEqual(parse_query('cmd 1, 2, 3'), ('cmd', ['1', '2', '3']))

    def test_command_with_many_params_and_spaces(self):
        self.assertEqual(parse_query('cmd 1 , 2 , 3'), ('cmd', ['1', '2', '3']))

    def test_command_with_one_param_with_spaces(self):
        self.assertEqual(parse_query('cmd Foo Bar'), ('cmd', ['Foo Bar']))

    def test_invalid_command(self):
        with self.assertRaises(ValueError):
            parse_query('cmd ,')


if __name__ == '__main__':
    unittest.main()
