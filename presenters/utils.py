import os
import re


def indent(multiline_str):
    """Remove a fixed amount of whitespace from the beginning of each line of a multi line string.

    This function removes the first and last line of the given string and then removes as much whitespace
    from the beginning of each line of the resulting string as there is its first line.

    Note:
        This function is intended as a helper to build multi line
        indented strings that are displayed as non-indented.

    Args:
        multiline_str (str): The string.

    Returns:
        (str): The input string, without the first and last lines and without leading whitespace
            on each line equal to the amount of leading whitespace of the first remaining line.

    Example:
        multiline_str = '''
            This text should be properly indented.
            It also contains multiple lines.
        ''')

        print(multiline_str)
            This text should be properly indented.
            It also contains multiple lines.

        print(indent(multiline_str))
        This text should be properly indented.
        It also contains multiple lines.
    """
    # Extract lines and skip the first and the last.
    lines = multiline_str.splitlines()[1:-1]

    # Get the length of the whitespace prefix of the first remaining line.
    prefix = re.search(r'\s+|$', lines[0]).group()
    k = len(prefix)

    # Strip that prefix from all lines and rebuild the resulting string.
    return '\n'.join(line[k:] for line in lines)


def touch_file(file_path):
    """Create a file if it does not exist.

    Note:
        This function does nothing if the given file already exists.

    Args:
        file_path (str): A string representing the path to a file.
    """
    if not os.path.exists(file_path):
        with open(file_path, 'w'):
            pass


def same_day(date1, date2):
    """Return `True` if the given dates represent the same day.

    Args:
        date1 (:obj:datetime.date): A date.
        date2 (:obj:datetime.date): Another date.

    Returns:
        (bool): `True` if the given dates represent the same day, `False` otherwise.
    """
    delta = date1 - date2
    return delta.days == 0


def parse_query(query):
    """Return the commands and the parameters in `query`.

    Note:
        The `query` string must be of the form 'cmd param1, param2, param3, ...'.
        This function returns a pair <'cmd', ['param1', 'param2', ...]>.

        An empty query corresponds to an empty command with no arguments.
        This function strips whitespace around commas.

    Args:
        query (str): A string of the form 'cmd param1, param2, param3, ...'.

    Returns:
        (:tuple:(str, :list:str)): A pair <command, list-of-arguments> in `query`.

    Raises:
        ValueError: If the given query contains empty parameters.
    """
    parts = query.split(maxsplit=1)
    if not parts:
        # An empty query corresponds to an empty command with no arguments.
        return '', []
    elif len(parts) == 1:
        # A single command, return it with no arguments.
        cmd, = parts
        return cmd, []
    else:
        cmd, params = parts
        params = re.sub(r'\s*,\s*', ',', params)

        params = params.split(',')
        if any(param == '' for param in params):
            raise ValueError('Query contains empty parameters')

        return cmd, params


def ask_yes_or_no():
    """Continuously prompt the user to choose either 'yes' or 'no', until it does."""
    answer = ''
    while answer not in ('yes', 'no'):
        answer = input('Please type `yes` or `no`... ').strip()
    return answer
