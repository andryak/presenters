import os
import random

from datetime import date
from datetime import datetime
from presenters.utils import indent
from presenters.utils import touch_file
from presenters.utils import ask_yes_or_no
from presenters.utils import parse_query
from presenters.utils import same_day


class Presenter:
    """A person identified by a unique name."""

    def __init__(self, name):
        """Create a new presenter with the given name.

        Args:
            name (str): The name of the presenter.
        """
        self.name = name

    def __hash__(self):
        """Return the hash value of this presenter.

        Returns:
            (int): The hash value of this presenter.
        """
        return hash(self.name)

    def __eq__(self, other):
        """Equality comparison operator.

        Args:
            other (object): The object to compare to this one for equality.

        Returns:
            (bool): `True` if the given object is also a presenter and has
                the same name of this presenter, `False` otherwise.
        """
        if not isinstance(other, Presenter):
            return False
        return self.name == other.name

    @classmethod
    def deserialize(cls, str_):
        """Create a new presenter from the given string.

        Note:
            The string must contain the name of the presenter.

        Args:
            str_ (str): The string containing the name of the presenter.

        Returns:
            (:obj:Presenter): A presenter object with name equal to `str_`.

        Example:
            Presenter.deserialize('Andrea Aquino')
        """
        return Presenter(str_)

    def serialize(self):
        """Return a serialized string representing this presenter.

        Note:
            The format returned by this method is the one accepted by `Presenter.deserialize`.

        Returns:
            (str): A serialized string representing this presenter.
        """
        return self.name

    def __str__(self):
        """Return the name of this presenter.

        Returns:
            (str): The name of this presenter.
        """
        return self.name

    def __repr__(self):
        """Return the representation of the name of this presenter.

        Returns:
            (str): The representation of the name of this presenter.
        """
        return repr(self.name)


class UnavailabilityLog:
    """A pair <presenter, date>.

    An UnavailabilityLog represents the unavailability of `presenter` for the meeting held at the date `date`.

    Note:
        This class handles dates represented as `datetime.date` objects.
        Incidentally, dates are in the yyyy-mm-dd format.
        This class has also a shorter alias: ULog.
    """

    def __init__(self, presenter, date_):
        """Create a new log.

        The log represents the unavailability of the given presenter for the meeting held at the given date.

        Args:
            presenter (:obj:Presenter): The presenter.
            date_ (:obj:datetime.date): The date on which the presenter is unavailable.
        """
        self.presenter = presenter
        self.date_ = date_

    def __hash__(self):
        """Return the hash value of this log."""
        return hash((self.presenter, self.date_))

    def __eq__(self, other):
        """Equality comparison operator.

        Args:
            other (object): The object to compare to this one for equality.

        Returns:
            (bool): `True` if the given object is also an `UnavailabilityLog` referencing
                the same presenter and the same date as this log, `False` otherwise.
        """
        if not isinstance(other, UnavailabilityLog):
            return False
        return self.presenter == other.presenter and self.date_ == other.date_

    @classmethod
    def deserialize(cls, str_):
        """Create a new log from the given string.

        Note:
            The string must contain the name of a presenter and a date
            in the format yyyy-mm-dd, separated by a newline character.

        Args:
            str_ (str): The string representing the log.

        Returns:
            (:obj:UnavailabilityLog): The log corresponding to the information represented by `str_`.

        Example:
            UnavailabilityLog.deserialize('Andrea Aquino\n1990-01-30')
        """
        presenter_str, date_str = str_.splitlines()
        presenter = Presenter(presenter_str)
        date_ = datetime.strptime(date_str, "%Y-%m-%d").date()
        return UnavailabilityLog(presenter, date_)

    def serialize(self):
        """Return a serialized string representing this log.

        Note:
            The format returned by this method is the one accepted by `UnavailabilityLog.deserialize`.

        Returns:
            (str): A serialized string representing this log.
        """
        presenter = self.presenter.name
        date_ = self.date_.strftime("%Y-%m-%d")
        return f'{presenter}\n{date_}'

    def __str__(self):
        """Return a human readable string describing this log.

        Returns:
            (str): A human readable string describing this log.
        """
        return f'{self.presenter} unavailable on {self.date_}'

    def __repr__(self):
        """Return the string representation of this log.

        Returns:
            (str): The string representation of this log.
        """
        return f'ULog(presenter={self.presenter!r}, date={self.date_!r})'


ULog = UnavailabilityLog
"""ULog is a shorter alias for UnavailabilityLog."""


class App:
    """Application to manage presenters at meetings.

    An application to manage potential presenters at meetings and keep track of their potential unavailability.
    """

    def __init__(self, presenters, ulogs):
        """Create a new application.

        The application manages the given set of presenters, taking into account the given set of unavailability logs.

        Args:
            presenters (:set:`Presenter`): the set of presenters tracked by the application.
            ulogs (:set:`UnavailabilityLog`): the set of unavailability logs tracked by the application.
        """
        self.presenters = presenters
        self.ulogs = ulogs

    def reset(self):
        """Clear both the set of the presenters and the unavailability logs of this application."""
        self.presenters.clear()
        self.ulogs.clear()

    def reset_presenters(self):
        """Clear the set of the presenters of this application."""
        self.presenters.clear()

    def reset_unavailability_logs(self):
        """Clear the unavailability logs of this application."""
        self.ulogs.clear()

    def presenter_exists(self, presenter):
        """Checks if a presenter is tracked by this application.

        Args:
            presenter (:obj:Presenter): The presenter.

        Returns:
            (bool): `True` if `presenter` exists in the set of presenters of this application, `False` otherwise.
        """
        return presenter in self.presenters

    def ulog_exists(self, ulog):
        """Checks if an unavailability log is tracked by this application.

        Args:
            ulog (:obj:UnavailabilityLog): The unavailability log.

        Returns:
            (bool): `True` if `ulog` exists in the set of unavailability logs of this application, `False` otherwise.
        """
        return ulog in self.ulogs

    def presenters_unavailable_today(self):
        """Return the set of presenters unavailable for today's meeting.

        Returns:
            (:set:Presenter): The set of presenters that are unavailable for today's meeting.
        """
        today = date.today()
        return {ulog.presenter for ulog in self.ulogs if same_day(ulog.date_, today)}

    def presenters_available_today(self):
        """Return the set of presenters available for today's meeting.

        Returns:
            (:set:Presenter): The set of presenters that are available for today's meeting.
        """
        return self.presenters - self.presenters_unavailable_today()

    @classmethod
    def load_presenters(cls, file):
        """Return the set of presenters stored in the given file.

        Note:
           The file must contain any number of presenters' names separated by a new line character.

        Args:
            file (:obj:io.TextIOBase): An open, readable file.

        Returns:
            (:set:Presenter): The set of presenters listed in the given file.
        """
        content = file.read()
        return {Presenter.deserialize(name) for name in content.splitlines()}

    @classmethod
    def load_unavailability_logs(cls, file):
        """Return the set of unavailability logs stored in the given file.

        Note:
            The file must contain any number of unavailability logs separated by a blank line.
            Each log must be in the format accepted by `UnavailabilityLog.deserialize`.

        Args:
            file (:obj:io.TextIOBase): An open, readable file.

        Returns:
            (:set:UnavailabilityLog): The set of unavailability logs listed in the given file.
        """
        # The if check prevents the split to return a single empty non-deserializable log.
        content = file.read()
        return set() if not content.strip() else {ULog.deserialize(str_) for str_ in content.split('\n\n')}

    def dump_presenters(self, file):
        """Clears the given file and writes the set of presenters to it.

        Note:
            Presenters are serialized by using `Presenter.serialize` and then separated by a newline character.

        Args:
            file (:obj:io.TextIOBase): An open, writable file.
        """
        file.write('\n'.join(presenter.serialize() for presenter in self.presenters))

    def dump_unavailability_logs(self, file):
        """Clears the given file and writes the set of unavailability logs to it.

        Note:
            Logs are serialized by using `UnavailabilityLog.serialize` and then separated by blank lines.

        Args:
            file (:obj:io.TextIOBase): An open, writable file.
        """
        file.write('\n\n'.join(ulog.serialize() for ulog in self.ulogs))

    def random_presenter(self):
        """Return a random presenter available to present at today's meeting.

        Returns:
            (:obj:Presenter): A random presenter available to present at today's meeting.

        Raises:
            IndexError: if no presenter is available for today's meeting.
        """
        available_presenters = self.presenters_available_today()

        # Transform available_presenters into a list since random.choice
        # assumes that the given sequence supports indexing.
        #
        # If the `available_presenters` set is empty this implicitly raises `IndexError`.
        return random.choice(list(available_presenters))

    def mark_unavailable(self, presenter, date_=None):
        """Mark the given presenter unavailable on the given date.

        Create and store an UnavailabilityLog declaring that the given presenter is not available on the given date.

        Note:
            If no date is provided, today's date is used.

        Args:
            presenter (:obj:Presenter): The presenter.
            date_ (:obj:datetime.date, optional): The date on which the presenter is unavailable.
                If no date is provided, today's date is used.

        Returns:
            (bool): `True` if the presenter was already unavailable on the given date, `False` otherwise.
        """
        date_ = date_ or date.today()
        ulog = ULog(presenter, date_)
        already_unavailable = ulog in self.ulogs
        self.ulogs.add(ulog)
        return already_unavailable

    def mark_available(self, presenter, date_=None):
        """Mark the given presenter available on the given date.

        Removes the UnavailabilityLog declaring that the given presenter is not available on the given date.

        Note:
            If no date is provided, today's date is used.

        Args:
            presenter (:obj:Presenter): The presenter.
            date_ (:obj:datetime.date, optional): The date on which the presenter is available.
                If no date is provided, today's date is used.

        Returns:
            (bool): `True` if the presenter was already available on the given date, `False` otherwise.
        """
        date_ = date_ or date.today()
        ulog = ULog(presenter, date_)
        already_available = ulog not in self.ulogs
        self.ulogs.discard(ulog)
        return already_available


def print_help():
    """Print a help message listing all available commands, their shortcuts and synopses."""
    print(indent("""
        Commands description and shortcuts
        ----------------------------------
          `h`, `?`, `help`: prints this help message.
          `q`, `quit`, `exit`: closes the application.
          `reset`: removes all presenters and unavailability logs stored by the application.
          `cp`, `clear-presenters`: removes all presenters stored by the application.
          `cl`, `clear-logs`: removes all unavailability logs stored by the application.
          `a`, `add`: creates one or more new presenters.
          `rm`, `remove`: removes one or more presenter.
          `lp`, `list-presenters`: prints the list of all presenters.
          `ll`, `list-logs`: prints the unavailability logs.
          `mu`, `mark-unavailable`: marks some presenters unavailable for the chosen day.
          `mu!`, `mark-unavailable-today`: marks some presenters unavailable for today's meeting.
          `ma`, `mark-available`: marks some presenters available for the chosen day.
          `ma!`, `mark-available-today`: marks some presenters available for today's meeting.
          `c`, `choose-presenter`: prints a randomly chosen presenter available for today's meeting.

        Commands synopses
        -----------------
          `help`
          `exit`
          `reset`
          `clear-presenters`
          `clear-logs`
          `add` presenter-name-1 [, presenter-name-2, ...]
          `remove` presenter-name-1 [, presenter-name-2, ...]
          `list-presenters`
          `list-logs`
          `mark-unavailable` date, presenter-name-1 [, presenter-name-2, ...]
          `mark-unavailable-today` presenter-name-1 [, presenter-name-2, ...]
          `mark-available` date, presenter-name-1 [, presenter-name-2, ...]
          `mark-available-today` presenter-name-1 [, presenter-name-2, ...]
          `choose-presenter`

        Examples and usage scenarios
        ----------------------------
          >>> reset
          Alert: this will clear all presenters and unavailability logs.
          Alert: you cannot undo this operation, are you sure?
          Please type `yes` or `no`... yes
          >>> list-presenters
          There are no presenters, you can add more using `add`.
          >>> choose-presenter
          Sorry, there are no available presenters.
          >>> add Andrea, Antonio, Silvia
          >>> list-presenters
          1) [A] Andrea
          2) [A] Antonio
          3) [A] Silvia
          >>> mark-unavailable-today Antonio
          >>> list-logs
          1) Antonio unavailable on 2017-11-18
          >>> list-presenters
          1) [A] Andrea
          2) [U] Antonio
          3) [A] Silvia
          >>> choose-presenter
          Andrea
          >>> exit
          See you soon!

    """))


def run_interactive_app():
    # Get the path to the current file.
    CURRENT_PATH = os.path.dirname(os.path.abspath(__file__))

    # Ensure `resources` child folder exists.
    RESOURCES_PATH = os.path.join(CURRENT_PATH, 'resources')
    if not os.path.exists(RESOURCES_PATH):
        os.makedirs(RESOURCES_PATH)

    # Reference helper files in the `resources` folder.
    PRESENTERS_FILE = os.path.join(RESOURCES_PATH, 'presenters.txt')
    ULOGS_FILE = os.path.join(RESOURCES_PATH, 'unavailability_logs.txt')

    # Ensure these helper files exist.
    touch_file(PRESENTERS_FILE)
    touch_file(ULOGS_FILE)

    # Load from the files the list of presenters and the unavailability logs.
    with open(PRESENTERS_FILE, 'r') as file:
        presenters = App.load_presenters(file)

    with open(ULOGS_FILE, 'r') as file:
        ulogs = App.load_unavailability_logs(file)

    # Instantiate the application.
    app = App(presenters, ulogs)

    def update_presenters_file():
        """Helper closure to update the presenters file."""
        with open(PRESENTERS_FILE, 'w') as file:
            app.dump_presenters(file)

    def update_ulogs_file():
        """Helper closure to update the unavailability logs file."""
        with open(ULOGS_FILE, 'w') as file:
            app.dump_unavailability_logs(file)

    # Print a welcoming message and a short usage guide.
    print(indent("""
        Welcome to presenters.py,
        an interactive application to choose presenters at meetings.

        Type `help` to see the full list of commands.
        Type `quit` to exit.
    """))

    # Enter interactive mode.
    while True:
        # Parse user input, if it does not match the required format print an error message and ask again.
        query = input('>>> ').strip()
        try:
            cmd, params = parse_query(query)
        except ValueError:
            print('Error: command does not match the required format.')
            print('Command format: cmd [param1] [, param2, ...]')
            continue

        if cmd == '':
            continue
        if cmd in ('h', '?', 'help'):
            # Handle help command.
            # Print an help message listing all commands, their descriptions, shortcuts and synopses.
            print_help()

        elif cmd in ('q', 'quit', 'exit'):
            # Handle exit command.
            # Greet the user and quit.
            print('See you soon!')
            exit(0)

        elif cmd == 'reset':
            # Handle reset command.
            # Potentially dangerous operation: ask for permission.
            print('Alert: this will clear all presenters and unavailability logs.')
            print('Alert: you cannot undo this operation, are you sure?')

            answer = ask_yes_or_no()
            if answer == 'no':
                print('Abort: the presenters lists and the unavailability logs will not be cleared.')
                continue

            # Reset the app and update the relevant files.
            app.reset()
            update_presenters_file()
            update_ulogs_file()

        elif cmd in ('cp', 'clear-presenters'):
            # Handle clear-presenters command.
            # Potentially dangerous operation: ask for permission.
            print('Alert: this will clear all presenters.')
            print('Alert: you cannot undo this operation, are you sure?')

            answer = ask_yes_or_no()
            if answer == 'no':
                print('Abort: the presenters list will not be cleared.')
                continue

            # Reset presenters and update the relevant file.
            app.reset_presenters()
            update_presenters_file()

        elif cmd in ('cl', 'clear-logs'):
            # Handle clear-logs command.
            # Potentially dangerous operation: ask for permission.
            print('Alert: this will clear all unavailability logs.')
            print('Alert: you cannot undo this operation, are you sure?')

            answer = ask_yes_or_no()
            if answer == 'no':
                print('Abort: the unavailability logs will not be cleared.')
                continue

            # Reset unavailability logs and update the relevant file.
            app.reset_unavailability_logs()
            update_ulogs_file()

        elif cmd in ('a', 'add'):
            # Handle add command.
            presenter_names = params
            if not presenter_names:
                print('Error: you must specify at least one presenter.')
                print(f'Command usage: `{cmd}` presenter-name1 [, presenter-name2, ...]')
                continue

            # Add new presenters to the app and update the relevant file.
            for presenter_name in presenter_names:
                presenter = Presenter(presenter_name)
                app.presenters.add(presenter)
            update_presenters_file()

        elif cmd in ('rm', 'remove'):
            # Handle remove command.
            presenter_names = params
            if not presenter_names:
                print('Error: you must specify at least one presenter.')
                print(f'Command usage: `{cmd}` presenter-name1 [, presenter-name2, ...]')
                continue

            # Remove presenters from the app and update the relevant file.
            for presenter_name in presenter_names:
                presenter = Presenter(presenter_name)
                if not app.presenter_exists(presenter):
                    print(f"Warning: presenter '{presenter.name}' does not exist and will be ignored.")
                app.presenters.discard(presenter)

            update_presenters_file()

        elif cmd in ('lp', 'list-presenters'):
            # Handle list-presenters command.
            if not app.presenters:
                print('There are no presenters, you can add more using `add`.')
                continue

            # Print presenters with availability information ordered by presenter name.
            presenters = sorted(app.presenters, key=lambda presenter: presenter.name)
            available_presenters = app.presenters_available_today()
            summary = []
            for i, presenter in enumerate(presenters, 1):
                status = 'A' if presenter in available_presenters else 'U'
                summary.append(f'{i}) [{status}] {presenter}')
            print('\n'.join(summary))

        elif cmd in ('ll', 'list-logs'):
            # Handle list-logs command.
            if not app.ulogs:
                print('There are no unavailability logs, you can add more using `mark-unavailable`.')
                continue

            # Print unavailability logs ordered by date.
            logs = sorted(app.ulogs, key=lambda ulog: ulog.date_)
            summary = []
            for i, ulog in enumerate(logs, 1):
                summary.append(f'{i}) {ulog}')
            print('\n'.join(summary))

        elif cmd in ('mu', 'mark-unavailable'):
            # Handle mark-unavailable command.
            if len(params) < 2:
                print('Error: you must specify at least one date and one presenter.')
                print(f'Command usage: `{cmd}` date, presenter-name1 [, presenter-name2, ...]')
                continue

            date_str, presenter_names = params[0], params[1:]
            try:
                date_ = datetime.strptime(date_str, "%Y-%m-%d").date()
            except ValueError:
                print('Error: the date must match the format yyyy-mm-dd.')
                continue
            else:
                for presenter_name in presenter_names:
                    presenter = Presenter(presenter_name)
                    if not app.presenter_exists(presenter):
                        print(f"Warning: presenter '{presenter.name}' does not exist and will be ignored.")
                        continue

                    already_unavailable = app.mark_unavailable(presenter, date_)
                    if already_unavailable:
                        print(f"Warning: '{presenter.name}' is already unavailable on date {date_str}.")

                update_ulogs_file()

        elif cmd in ('mu!', 'mark-unavailable-today'):
            # Handle mark-unavailable-today command.
            presenter_names = params
            if not presenter_names:
                print('Error: you must specify at least one presenter.')
                print(f'Command usage: `{cmd}` presenter-name1 [, presenter-name2, ...]')
                continue

            for presenter_name in presenter_names:
                presenter = Presenter(presenter_name)
                if not app.presenter_exists(presenter):
                    print(f"Warning: presenter '{presenter.name}' does not exist and will be ignored.")
                    continue

                already_unavailable = app.mark_unavailable(presenter)
                if already_unavailable:
                    print(f"Warning: '{presenter.name}' is already unavailable today.")

            update_ulogs_file()

        elif cmd in ('ma', 'mark-available'):
            # Handle mark-available command.
            if len(params) < 2:
                print('Error: you must specify at least one date and one presenter.')
                print(f'Command usage: `{cmd}` date, presenter-name1 [, presenter-name2, ...]')
                continue

            date_str, presenter_names = params[0], params[1:]
            try:
                date_ = datetime.strptime(date_str, "%Y-%m-%d").date()
            except ValueError:
                print('Error: the date must match the format yyyy-mm-dd.')
                continue
            else:
                for presenter_name in presenter_names:
                    presenter = Presenter(presenter_name)
                    if not app.presenter_exists(presenter):
                        print(f"Warning: presenter '{presenter.name}' does not exist and will be ignored.")
                        continue

                    already_available = app.mark_available(presenter, date_)
                    if already_available:
                        print(f"Warning: '{presenter.name}' is already available on date {date_str}.")

                update_ulogs_file()

        elif cmd in ('ma!', 'mark-available-today'):
            # Handle mark-available-today command.
            presenter_names = params
            if not presenter_names:
                print('Error: you must specify at least one presenter.')
                print(f'Command usage: `{cmd}` presenter-name1 [, presenter-name2, ...]')
                continue

            for presenter_name in presenter_names:
                presenter = Presenter(presenter_name)
                if not app.presenter_exists(presenter):
                    print(f"Warning: presenter '{presenter.name}' does not exist and will be ignored.")
                    continue

                already_available = app.mark_available(presenter)
                if already_available:
                    print(f"Warning: '{presenter.name}' is already available today.")

            update_ulogs_file()

        elif cmd in ('c', 'choose-presenter'):
            # Handle choose-presenter command.
            try:
                random_presenter = app.random_presenter()
            except IndexError:
                print('Sorry, there are no available presenters.')
            else:
                print(random_presenter)

        else:
            # Handle unrecognized commands.
            print('Error: unrecognized command.')
            print('Please type `help` or `?` to see the full list of commands.')


if __name__ == '__main__':
    run_interactive_app()
